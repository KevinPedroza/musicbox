<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cancion extends Model
{
    protected $fillable = ['nombre', 'formato', 'proceso', 'link', 'hash'];
}
