<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\cancion;
use CloudConvert; 
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Flavy;

class convertir extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:convertir {id} {hash} {formato} {nombre}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Consulta si la cancion se encuntra registrada en la base de datos        
        $cancion = cancion::where('hash', $this->argument('hash'))->where('formato', $this->argument('formato'))->get('link');
        $object = json_decode($cancion, true);

        $download = 'http://127.0.0.1:8100/api/descarga/'.$this->argument('hash').$this->argument('formato');
        $conexion1 = mysqli_connect("localhost", "root", "", "cli_musicbox");

        if($object[0]["link"] === null || $object[0]["link"] === ""){
            //convierte la cancion a el nuevo formato
            $cmd = 'C:/ffmpeg-4.1.3-win64-static/bin/ffmpeg.exe -i http://127.0.0.1:8887/storage/app/public/canciones/' . $this->argument('nombre') . ' C:/Users/kenda/Documents/Materias_Año_2019/II_Cuatrimestre/Ingenieria_de_Software_Libre/Proyecto_1/musicbox/cli_api/public/canciones/' . $this->argument('hash') . $this->argument('formato') . ' 2>&1';
            exec($cmd, $output, $value); //ejecuta el comando
            Log::info(var_dump($output));
            Log::info(var_dump($value));
            Log::info("ffmpeg -i http://127.0.0.1:8887/storage/app/public/canciones/" . $this->argument('nombre') . " C:/Users/kenda/Documents/Materias_Año_2019/II_Cuatrimestre/Ingenieria_de_Software_Libre/Proyecto_1/musicbox/cli_api/public/canciones/" . $this->argument('hash') . $this->argument('formato') . " 2>&1");

            
            $sql_statement = "INSERT INTO cancion_link (hash_cancion, formato, link) VALUES " . "('" . $this->argument('hash') . "', '" . $this->argument('formato') . "', '" . $download . "')";
            $verifi = mysqli_query($conexion1, $sql_statement);
            
            if($verifi){
                //esto es para actulizar el proceso y agregar el link de descarga a la cancion
                cancion::where('id', $this->argument('id'))->where('formato', $this->argument('formato'))->update(['link' => 'http://127.0.0.1:8100/api/descarga/'.$this->argument('hash').$this->argument('formato'), 'proceso'=>'Terminado']);
            }
            else{
                Log::info(var_dump("Error no se guardo"));
            }
        }
        else{
            $link2 = $object[0]['link'];
            $hash2 = $this->argument('hash');

            $sql_statement2 = "UPDATE `cancion_link` SET link = '$link2' WHERE hash_cancion = '$hash2';";

            $verifi2 = mysqli_query($conexion1, $sql_statement2);
            
            if($verifi2){
                //esto es para actulizar el proceso y agregar el link de descarga a la cancion
                cancion::where('id', $this->argument('id'))->where('formato', $this->argument('formato'))->update(['link' => $link2, 'proceso'=>'Terminado']);
            }
            else{
                Log::info(var_dump("Error no se pudo modificar el estudiante selecionado."));
            }
        }
    }
}
