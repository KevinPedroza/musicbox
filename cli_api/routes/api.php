<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//esta ruta es para realizar la descarga del archivo 
Route::get('/descarga/{cancion}', 'cancionController@downloads');

//esta ruta es para convertir el archivo
Route::post('/convertir', function(Request $request){
    $id = $request->get('id');
    $hash = $request->get('hash');
    $formato = $request->get('formato');
    $nombre = $request->get('nombre');

    //llama a el archivo que ejecuta el comando para pasarle los parametros
    Artisan::call('command:convertir',['id'=> $id, 'hash'=> $hash, 'formato'=>$formato, 'nombre'=>$nombre]);
});