<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function(){
    //aqui van todas las rutas que necesitan CORS
    
    Route::get('cancion', 'cancionController@index');
    Route::get('cancion/{id}','cancionController@show');
    Route::post('cancion/{formato}','cancionController@store');
    Route::put('cancion/{id}','cancionController@update'); 
    Route::delete('cancion/{id}','cancionController@delete'); 
}); 