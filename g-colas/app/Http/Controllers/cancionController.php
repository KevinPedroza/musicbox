<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cancion;
use App\Jobs\ProcessCancion;

class cancionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cancion = Cancion::all();

        return response()->json($cancion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $formato) 
    {

        $cancion = new Cancion;
        
        if ($request->hasFile('data')) {
            $file = $request->file('data');

        }else{
            return response()->json($formato, 201);
        }

        $path = storage_path() . '/app/' . 'public' . "/canciones/";
        $file->move($path, $file->getClientOriginalName());

        $cancion->nombre = $file->getClientOriginalName();
        $cancion->formato = $formato;
        $cancion->proceso = 'En Proceso..';
        $cancion->link = '';
        $cancion->hash = sha1_file($path . $file->getClientOriginalName());
        $cancion->save();

        $id = Cancion::all()->last()->id;
        $nombre = $file->getClientOriginalName();
        $hash = sha1_file($path . $file->getClientOriginalName());
        
        ProcessCancion::dispatch($id, $nombre, $hash, $formato)
                ->delay(now()->addSeconds(30)); /// dispatchNow  addMinutes
                
        return response()->json($cancion, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cancion::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cancion = Cancion::findOrFail($id);
        $cancion->update($request->all());

        return response()->json([
            'message' => 'cancion editada',
            'cancion' => $cancion
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cancion = Cancion::findOrFail($id);
        $cancion->delete();

        return 204;
    }
}
