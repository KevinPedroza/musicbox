<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;

class ProcessCancion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

     protected $id;
     protected $nombre;
     protected $hash;
     protected $formato;

     public $deleteWhenMissingModels = true;

    public function __construct(  $id, $nombre, $hash, $formato  )
    {
        $this->id=$id;
        $this->nombre=$nombre;
        $this->hash=$hash;
        $this->formato=$formato;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $endpoint = 'http://127.0.0.1:8100/api/convertir';
        $options = [
            'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'],
            'json' => [
                'id' => $this->id,
                'nombre' => $this->nombre,
                'hash' => $this->hash,
                'formato' => $this->formato],
             ];

        $client = new Client();
        $client->post($endpoint, $options);
    }
}
