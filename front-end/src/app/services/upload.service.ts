import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cancion } from './../modelos/cancion'

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  pushFileToStorage(file: File, formato: string, nombre: string): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();

    formdata.append('data', file);

    const req = new HttpRequest('POST', 'http://127.0.0.1:8000/api/cancion/' + formato, formdata, {
      reportProgress: true,
      responseType: 'text',
    });

    return this.http.request(req);
  }

  getCanciones(){
    return this.http.get<Cancion[]>('http://127.0.0.1:8000/api/cancion/');
  }

}
