import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ErrorComponent } from './error/error.component';
import { HomeRoutingModule } from './home-routing.module';
import { Ng2IziToastModule, Ng2IzitoastService } from 'ng2-izitoast';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [InicioComponent, ErrorComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    Ng2IziToastModule
  ]
})
export class HomeModule { }
