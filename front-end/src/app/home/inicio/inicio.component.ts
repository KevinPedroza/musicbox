import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { UploadService } from "./../../services/upload.service";
import { Ng2IzitoastService } from 'ng2-izitoast';
import { Cancion } from './../../modelos/cancion';
import { ToastrService } from 'ngx-toastr';
import { SimpleTimer } from 'ng2-simple-timer';
import * as Driver from 'driver.js'

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit { 

  selectedFiles: FileList;
  currentFileUpload: File;
  song: string;
  cancion: any;
  formato: string;
  proceso: boolean = false;
  nombre: string;
  canciones: Cancion[] = [];
  size_cancion: number;
  subir: boolean = true;
  timerId: string;


  constructor(private uploadService: UploadService, public iziToast: Ng2IzitoastService, private toastr: ToastrService, private st: SimpleTimer) { }

  ngOnInit() {
    //this.getCanciones();
    this.st.newTimer('30sec',30)
    this.timerId = this.st.subscribe('30sec', () => this.getCanciones());

    const driver = new Driver({
      animate: true,  // Animate while changing highlighted element // Distance of element from around the edges
      allowClose: true, // Whether clicking on overlay should close or not
      overlayClickNext: false, // Should it move to next step on overlay click
      doneBtnText: 'Hecho!', // Text on the final button
      closeBtnText: 'Cerrar', // Text on the close button for this step
      nextBtnText: 'Siguiente', // Next button text for this step
      prevBtnText: 'Anterior',
    });
    driver.defineSteps([
      {
        element: '#song',
        popover: {
          title: 'Seleccionar Canción',
          description: 'Acá puedes seleccionar una canción que este en tu computadora',
          position: 'bottom'
        },
      },
      {
        element: '#formato',
        popover: {
          title: 'Selecciona un Formato',
          description: 'Aca puedes seleccionar un formato de canción a convertir',
          position: 'top'
        }
      },
      {
        element: '#subir',
        popover: {
          title: 'Convertir Canción',
          description: 'Presionando en el botón, comenzarás a convertir tu Canción!',
          position: 'bottom'
        }
      },
      {
        element: '#lista',
        popover: {
          title: 'Descargar Canción',
          description: 'Acá podrás encontrar tus canciones, las cuales podrás descargar!',
          position: 'center'
        }
      },
    ]);
    driver.start();
  }

  Proceso(){
    this.proceso = !this.proceso;
    this.subir = !this.subir;
  }

  onUpload() {
    console.log(this.formato);
    this.Proceso();
    this.currentFileUpload = this.selectedFiles.item(0);
    console.log(this.currentFileUpload);
    if(this.size_cancion < 10000000){ //10000000 esto es igual a 10 MB
    
      this.uploadService.pushFileToStorage(this.currentFileUpload, this.formato, this.nombre).subscribe(event => {
        if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
          this.formato = null;
          this.getCanciones();
          this.Proceso();
        } },
        error =>{
          this.formato = null;
          this.getCanciones();
          this.Proceso();
        });
      //this.selectedFiles = undefined;
    }else{
      this.toastr.warning('El archivo supera el limite del tamaño permitido.', 'Advertencia');
      console.log('El archivo supera el limite del tamaño permitido.');
    }
  }

  onFileSelected(event) {
    this.selectedFiles = event.target.files; 
    console.log(event);
    if(this.selectedFiles.item(0) == undefined){
      this.iziToast.error({title: "Error",
      message: 'Selecciona una Cancion!', position: 'topRight'});
    }else{
      this.song = this.selectedFiles.item(0).type.split('/')[0];
      this.size_cancion = this.selectedFiles.item(0).size;
      console.log(this.size_cancion + " tamaño en bytes.");
      
      if(this.song != 'audio'){
        this.iziToast.error({title: "Error",
        message: 'Selecciona una Cancion!', position: 'topRight'});
        this.cancion = null;
      }
      this.nombre = this.selectedFiles.item(0).name;
    }
  }

  getCanciones(){
    this.uploadService.getCanciones().subscribe(data => { 
      console.log(data);
      this.canciones = data;
    });
  }
}
