export class Cancion{
    id: number;
    nombre: string;
    formato: string;
    hash: string;
    proceso: string;
    link: string;
}