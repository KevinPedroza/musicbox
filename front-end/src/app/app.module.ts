import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UploadService } from "./services/upload.service";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { HomeRoutingModule } from './home/home-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2IziToastModule, Ng2IzitoastService } from 'ng2-izitoast';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { SimpleTimer } from 'ng2-simple-timer';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    HttpClientModule,
    Ng2IziToastModule,
    AppRoutingModule,
    HomeRoutingModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [UploadService, Ng2IzitoastService, SimpleTimer],
  bootstrap: [AppComponent]
})
export class AppModule { }
